<?php

// Dashboard > videos
Breadcrumbs::for('admin.videos', function ($trail) {
//    $trail->parent('dashboard');
    $trail->push(__('general.Videos'), route('admin.videos'));
});

// Dashboard > trainings > create
Breadcrumbs::for('admin.videos.create', function ($trail) {
    $trail->parent('admin.videos');
    $trail->push(__('general.Create Video'), route('admin.videos.create'));
});

// Dashboard > videos > edit
Breadcrumbs::for('admin.videos.edit', function ($trail, $video) {
    $trail->parent('admin.videos');
    $trail->push(__('general.Edit Video'), route('admin.videos.edit', $video->title));
});

