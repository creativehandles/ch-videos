<?php

//videos related
Route::group(['name' => 'videos', 'groupName' => 'videos'], function () {
    //videos plugin related routes for backend
    Route::get('/videos', 'PluginsControllers\VideosController@index')->name('videos');
    Route::get('/videos/create', 'PluginsControllers\VideosController@create')->name('videos.create');
    Route::post('/videos/create', 'PluginsControllers\VideosController@store')->name('videos.store');
    Route::get('/videos/edit/{id}', 'PluginsControllers\VideosController@edit')->name('videos.edit');
    Route::put('/videos/update/{id}', 'PluginsControllers\VideosController@update')->name('videos.update');
    Route::delete('/videos/delete/{id}', 'PluginsControllers\VideosController@destroy')->name('videos.delete');
    Route::get('/get-ajax-videos', 'PluginsControllers\VideosController@searchItem')->name('ajaxSearchVideos');
});