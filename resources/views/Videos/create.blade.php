@extends('Admin.layout')

@section("styles")
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet"/>
    <style>
        .input-group .select2-container {
            position: relative;
            z-index: 2;
            float: left;
            width: 80% !important;
            margin-bottom: 0;
            display: table;
            table-layout: fixed;
        }
    </style>
@endsection

@section('content')
    @include('Admin.partials.breadcumbs',['header'=>__('general.Add a new video')])
    @include('Admin.partials.form-alert')

    <div class="content-body">
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title" id="basic-layout-form-center">{{__('videosplugin.Add a new video')}}</h4>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <form action="{{route('admin.videos.store')}}" method="post" id="trainingForm"
                                  enctype="multipart/form-data">

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-body">
                                            {{csrf_field()}}
                                            {{--title--}}
                                            <div class="form-group">
                                                <label>{{__('videosplugin.Video Title')}}</label>
                                                <input required type="text" class="form-control" id="video_title"
                                                       placeholder="{{__('videosplugin.Video Title')}}"
                                                       name="video_title" value="{{old('video_title')}}">
                                            </div>
                                            <input type="hidden" name="source_id" id="id">
                                            {{--featured media type--}}
                                            <div class="form-group">
                                                <label>{{__('videosplugin.Type')}}</label>
                                                <fieldset class="radio">
                                                    <label>
                                                        <input type="radio" checked required name="type"
                                                               value="img" class="type">
                                                        {{__('videosplugin.Image (For e books)')}}
                                                    </label>
                                                </fieldset>
                                                <fieldset class="radio">
                                                    <label>
                                                        <input type="radio" required name="type" value="vdo"
                                                               class="type">
                                                        {{__('videosplugin.Video')}}
                                                    </label>
                                                </fieldset>
                                            </div>
                                            {{--featured image--}}
                                            <div class="form-group hide" id="img-container">
                                                <label>{{__('trainings.form.feature_img')}}</label>
                                                <input id="image" type="file" class="form-control"
                                                       name="image">
                                                <img id="featured_image_preview" class="hide height-150 img-thumbnail">
                                            </div>
                                            <div class="hide" id="vdo-container">
                                            {{--featured video--}}
                                            <div class="form-group" >
                                                <label>{{__('trainings.form.feature_video')}}</label>
                                                <input id="video" type="text" class="form-control" value="{{old('feature_video')}}"
                                                       placeholder="{{__('trainings.form.video_placeholder')}}" name="video">
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{__('videosplugin.Video duration')}}</label>
                                                        <input type="text" name="video_duration" class="form-control"
                                                               value="{{old('video_duration')}}"
                                                               placeholder="{{__('videosplugin.Video duration')}}">
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                            {{--about video--}}
                                            <div class="form-group">
                                                <label>{{__('videosplugin.Video description')}}</label>
                                                <textarea name="description" id="description" cols="30" rows="10"
                                                          placeholder="{{__('videosplugin.Video description')}}"
                                                          class="form-control summernote-body">{{old('description')}}</textarea>
                                            </div>

                                            {{--attachments--}}
                                            <div class="form-group">
                                                <div class="form-group mb-2 attachment-repeater">
                                                    <label>{{__('videosplugin.Attachments')}}</label>
                                                    <div data-repeater-list="repeater-group-attachments">
                                                        <div class="row "data-repeater-item>
                                                            <div class="col-md-4">
                                                                <div class="form-group mb-1" >
                                                                    <input type="file" class="form-control"
                                                                           name="attachment">

                                                                </div>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <div class="input-group-append">
                                                                    <div class="input-group mb-1" >
                                                                        <input type="text" class="form-control"
                                                                               name="attachment-name">
                                                                    </div>
                                                                        <span class="input-group-btn" id="button-addon2">
                                                                            <button class="btn btn-danger" type="button" data-repeater-delete><i class="ft-x"></i></button>
                                                                        </span>
                                                                </div>
                                                            </div>
                                                        </div>


                                                    </div>
                                                    <button type="button" id="repeater-reference-btn" data-repeater-create class="btn btn-primary">
                                                        <i class="icon-plus4"></i> {{__('videosplugin.Add new attachment')}}
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="form-actions left">
                                                <button type="reset" class="btn btn-warning mr-1">
                                                    <i class="ft-x"></i> {{__('trainings.general.reset')}}
                                                </button>

                                                <a href="{{ URL::previous() }}">
                                                    <button type="button" href="" class="btn btn-warning mr-1">
                                                        <i class="ft-arrow-left"></i> {{__('trainings.general.goBack')}}
                                                    </button></a>
                                                <button type="button" class="btn btn-primary" id="saveForm">
                                                    <i class="fa fa-check-square-o"></i> {{__('trainings.general.update')}}
                                                </button>
                                                <button type="submit" class="btn btn-primary" id="saveAndGoBack">
                                                    <i class="fa fa-check-square-o"></i> {{__('trainings.general.updateAndBack')}}
                                                </button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4"></div>
        </div>
    </div>
@endsection

@section("scripts")
    <script src="{{ asset("vendors/js/forms/repeater/jquery.repeater.min.js")}}"></script>
    <script src="{{ asset("vendors/js/forms/extended/inputmask/jquery.inputmask.bundle.min.js")}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
    <script src="{{ asset("js/scripts/video_scripts.js")}}"></script>
@endsection