@extends('Admin.layout')

@section("styles")

@endsection

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    @include('Admin.partials.breadcumbs',['header'=> __('videosplugin.Video')])
                </div>
            </div>
        </div>

        <div class="content-header-right col-md-6 col-12 mb-2">
            <div class="mb-1 pull-right">
                <a href="{{route('admin.videos.create')}}"
                   class="btn btn-secondary btn-block-sm"><i
                            class="ft-file-plus"></i> {{ __('videosplugin.Create videos') }}</a>

            </div>
        </div>
    </div>
    @include('Admin.partials.form-alert')

    @if($videos->count() > 0)
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <form action="" id="">
                                <div class="form-body">

                                    <div class="form-group">
                                        {{--<input type="text" name="filter" class="form-control" placeholder="">--}}
                                        <label for="searchItemSelect" class="display-inline-block mb-1">{{ __('videosplugin.Search For Videos') }}
                                        </label>
                                        <select name="search" class="select2 form-control select2-hidden-accessible"
                                                tabindex="-1" aria-hidden="true"
                                                id="searchItemSelect">
                                        </select>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    @endif

    <div class="content-body">
        <div class="row">
            @if($videos->count() <= 0)
                <div class="col-md-12 text-center">
                    <p class="text-center">{{ __('videosplugin.No videos to display. Want to create a new one? Click the button below') }} </p>
                    <a href="{{route('admin.videos.create')}}" class="btn btn-primary"> {{ __('videosplugin.Create a video') }}</a>
                </div>
            @else
                <div class="col-md-12">
                    <div class="row match-height">
                        @foreach($videos as $video)
                            <div class="col-xl-4 col-md-6 col-lg-4 col-sm-12">
                                <div class="card">
                                    <div class="card-content">
                                        <img class="card-img-top img-fluid height-200 object_cover"
                                             src="{{$video->featured_media}}"
                                             alt="Card image cap">
                                        <div class="card-body">
                                            <h4 class="card-title">{{$video->title}}</h4>

                                            <div class="row match-height">
                                                <div class="col">
                                                    <div class="btn-group pull-right" role="group"
                                                         aria-label="Basic example">
                                                        <a class="btn btn-md btn-outline-primary"
                                                           href="{{route('admin.videos.edit',['id'=>$video->id])}}"><i
                                                                    class="fa fa-edit"></i></a>
                                                        <a class="btn btn-md btn-outline-danger delete-video"
                                                           data-id="{{$video->id}}"
                                                           href=""><i class="fa fa-trash"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-md-12 pagination-col center">
                    {{ $videos->links() }}
                </div>
            @endif
        </div>
    </div>
@endsection

@section("scripts")
    <script src="{{ asset("vendors/js/extensions/sweetalert.min.js") }}" type="text/javascript"></script>
    <script>
        $(document).ready(function(){
            var url = '{{route('admin.ajaxSearchVideos')}}';

            $("#searchItemSelect").select2({
                minimumInputLength: 2,language:"{{ app()->getLocale() }}",
                allowClear: true,
                placeholder: "{{ __('videosplugin.Search for videos') }}",
                ajax: {
                    url: url,
                    dataType: 'json',
                    type: "GET",
                    quietMillis: 50,
                    data: function (term) {
                        return {
                            term: term
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    },
                    // cache: true
                }
            });
            var editItem = "{{ route('admin.videos.edit',['id'=>'sampleId']) }}";
            $('#searchItemSelect').on('select2:select', function (e) {
                var data = e.params.data;
                location.href = editItem.replace('sampleId',data.id);
            });

            $('#searchItemSelect').on('blur', function (e) {
                e.preventDefault();
                $('#searchItemSelect').select2("close")
            });
        });
    </script>
    <script>
        $('.delete-video').on('click', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            var deleteVideo = "{{ route('admin.videos.delete',['id'=>'sampleId']) }}";
            swal({
                title: "{{__('general.Warning!')}}",
                text: "{{__('general.Are you sure you need to delete this item? this change cannot be undone.')}}",
                icon: "warning",
                buttons: {
                    cancel: {
                        text: "{{__('general.Cancel')}}",
                        value: null,
                        visible: !0,
                        className: "",
                        closeModal: !1
                    },
                    confirm: {
                        text: "{{__('general.Yes.Delete')}}",
                        value: !0,
                        visible: !0,
                        className: "",
                        closeModal: !1
                    }
                }
            }).then(e => {
                if (e) {
                    $.ajax({
                        dataType: 'json',
                        method: 'delete',
                        url: deleteVideo.replace('sampleId',id),
                    }).done(function (response) {
                        swal("{{__('general.Success!')}}", "{{__('general.Item deleted!!')}}", "success").then(() => {
                            location.reload();
                        });
                    }).fail(function (erroErrorr) {
                        swal("{{__('general.Error')}}", "{{__('general.Error Occured')}}", "error");
                    });
                } else {
                    swal("{{__('general.Cancelled')}}", "{{__("general.It's safe")}}", "error");
                }
            });

        });
    </script>
@endsection