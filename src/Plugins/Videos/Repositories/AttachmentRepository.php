<?php
/**
 * Created by PhpStorm.
 * User: deemantha
 * Date: 8/7/19
 * Time: 8:33 PM
 */

namespace Creativehandles\ChVideos\Plugins\Videos\Repositories;


use Creativehandles\ChVideos\Plugins\Videos\Models\AttachmentModel;
use App\Repositories\BaseEloquentRepository;

class AttachmentRepository extends BaseEloquentRepository
{

    public function __construct(AttachmentModel $model)
    {
        $this->model = $model;
    }


}