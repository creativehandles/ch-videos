<?php
/**
 * Created by PhpStorm.
 * User: deemantha
 * Date: 8/7/19
 * Time: 8:33 PM
 */

namespace Creativehandles\ChVideos\Plugins\Videos\Repositories;


use Creativehandles\ChVideos\Plugins\Videos\Models\VideoModel;
use App\Repositories\BaseEloquentRepository;

class VideoRepository extends BaseEloquentRepository
{

    public function __construct(VideoModel $model)
    {
        $this->model = $model;
    }


}