<?php

namespace Creativehandles\ChVideos\Http\Controllers\PluginsControllers;

use Creativehandles\ChVideos\Plugins\Videos\Videos;
use App\Traits\UploadTrait;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Validator;

class VideosController extends Controller
{

    protected $views = 'Admin.Videos.';
    protected $logprefix = "VideosController";
    /**
     * @var Videos
     */
    private $videos;

    use UploadTrait;

    public function __construct(Videos $videos)
    {
        $this->videos = $videos;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $videos = $this->videos->getAllVideos();
        return view($this->views . "index")->with(['videos' => $videos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->views . "create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //get the request
        $formData = $request->all();

        $validator = $this->validateForm($request);

        if ($validator->fails()) {
            Log::error($this->logprefix . '(' . __FUNCTION__ . '): Videos data incomplete ', $request->all());

            if ($request->ajax()) {
                return response()->json(['msg' => $validator->errors()->toArray()], 400);
            } else {
                return redirect()->back()->withInput()->withErrors($validator);
            }
        }

        //format video data
        $formattedForm = [];

//        $formattedForm["id"] = $formData['source_id'];

        $formattedForm = $this->formatFormData($request, $formData, $formattedForm);

        try {

            if($request->get('source_id',null)){
                $formattedForm['id'] = $request->get('source_id');
                $storedVideo = $this->videos->createOrUpdateVideo($formattedForm);

            }else{
                //save it in the database
                $storedVideo = $this->videos->createOrUpdateVideo($formattedForm);

            }

            //process attachments after storing video
            $attachmentList = $this->multipleFileUpload($request, 'repeater-group-attachments', $formattedForm["title"],
                '/uploads/trainings/' . $formattedForm["author"] . '/', 'attachments');

            if ($storedVideo && count($attachmentList) > 0) {
                foreach ($attachmentList as $key => $attachment) {
                    $data[] = [
                        'video_id' => $storedVideo['id'],
                        'attachment_url' => $attachment,
                        'attachment_name' => $request->get('repeater-group-attachments')[$key]['attachment-name'] ?? 'attachment-'.$key
                    ];
                }

                $storedVideo->attachments()->delete();
                $storedVideo->attachments()->insert($data);
            }
        } catch (Exception $e) {
            if ($request->ajax()) {
                return response()->json(['msg' => "Error Occured", 'developer_msg' => $e->getMessage()], 500);
            } else {
                return redirect()->back()->withInput()->with('error',
                    'Error Occured :' . $e->getMessage())->withInput();
            }
        }

        if ($request->ajax()) {
            return response()->json(['msg' => "Video Saved", 'data' => $storedVideo], 200);
        } else {
            return redirect()->route('admin.videos')->with('success', 'Video added successfully!');
        }


    }

    protected function validateForm($request)
    {
        $rules = [
            'video_title' => 'required',
            'type' => 'required',
            'description' => 'required',
        ];


        $validator = Validator::make($request->all(), $rules);
        return $validator;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $video = $this->videos->findVideoById($id);
        return view($this->views . "edit")->with(compact('video'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //get the request
        $formData = $request->all();

        $validator = $this->validateForm($request);

        if ($validator->fails()) {
            Log::error($this->logprefix . '(' . __FUNCTION__ . '): Videos data incomplete ', $request->all());

            if ($request->ajax()) {
                return response()->json(['msg' => $validator->errors()->toArray()], 400);
            } else {
                return redirect()->back()->withInput()->withErrors($validator);
            }
        }

        //format video data
        $formattedForm = [];
        $formattedForm = $this->formatFormData($request, $formData, $formattedForm);
        $formattedForm["id"] = $formData['source_id'];

        try {

            $storedVideo = $this->videos->createOrUpdateVideo($formattedForm);

            //process attachments after storing video
            $attachmentList = $this->multipleFileUpload($request, 'repeater-group-attachments', $formattedForm["title"],
                '/uploads/trainings/' . $formattedForm["author"] . '/', 'attachments');

            $formAttachments = $request->get('repeater-group-attachments');
            $formAttachedItems = array_column($formAttachments, 'attachment');
            $currentAttachments = $storedVideo->attachments->pluck('id')->all();
//            dd($formAttachments);
            foreach ($formAttachments as $key=>$array){
                if(array_get($array,'attachment',null)  && array_get($array,'attachment-name',null)){
                    $names = [
                        'video_id' => $storedVideo['id'],
                        'attachment_name' => array_get($array,'attachment-name','attachment-'.$key)
                    ];
                    $storedVideo->attachments()->where('id',array_get($array,'attachment',null))->update($names);
                }
            }

            //get difference and delete that
            $deleteItems = array_diff($currentAttachments, $formAttachedItems);
            $storedVideo->attachments()->whereIn('id', $deleteItems)->delete();

            //insert new files
            if ($storedVideo && count($attachmentList) > 0) {
                foreach ($attachmentList as $key => $attachment) {
                    $data[] = [
                        'video_id' => $storedVideo['id'],
                        'attachment_url' => $attachment,
                        'attachment_name' => $request->get('repeater-group-attachments')[$key]['attachment-name']?? 'attachment-'.$key
                    ];
                }
                $storedVideo->attachments()->insert($data);
            }

            //update older files

        } catch (Exception $e) {
            if ($request->ajax()) {
                return response()->json(['msg' => "Error Occured", 'developer_msg' => $e->getMessage()], 500);
            } else {
                return redirect()->back()->withInput()->with('error',
                    'Error Occured :' . $e->getMessage())->withInput();
            }
        }

        if ($request->ajax()) {
            return response()->json(['msg' => "Video Saved", 'data' => $storedVideo], 200);
        } else {
            return redirect()->route('admin.videos')->with('success', 'Video added successfully!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deletedVideo = $this->videos->deleteVideoById($id);

        if (!$deletedVideo) {
            return response()->json(['msg' => "Error Occured"], 400);
        }

        return response()->json(['msg' => "Page Deleted"], 200);
    }

    /**
     * @param Request $request
     * @param $formData
     * @param $formattedForm
     * @return mixed
     */
    protected function formatFormData(Request $request, $formData, $formattedForm)
    {
        $formattedForm["title"] = $formData['video_title'];
        $formattedForm["type"] = $formData['type'];
        $formattedForm["video"] = $formData['video'];
        $formattedForm["duration"] = $formData['video_duration'];
        $formattedForm["description"] = $formData['description'];
        $formattedForm["author"] = Auth::user()->getAuthIdentifier();

        $imgPath = $this->processUpload($request, 'image', 'trainings', $formattedForm["title"]);

        if ($imgPath) {
            $formattedForm["image"] = $imgPath;
        }
        return $formattedForm;
    }

    public function searchItem(Request $request)
    {
        $param = $request->input('term');

        $page = $this->videos->getVideosByName(array_get($param, 'term', ''));

        $page = $page->map(function ($page) {

//            $page->id = $page->trainings_id;
            $page->text = $page->title;

            return collect($page->toArray())
                ->only(['id', 'text'])
                ->all();
        });
        return response()->json($page);
    }
}
